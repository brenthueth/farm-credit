# README

## Production Credit and the Dust Bowl

**Collaborators:** Professor Brent Hueth, Jared Hutchins

**E-mail:** brent.hueth@wisc.edu, jhutchins@wisc.edu

Department of Agricultural and Applied Economics

University of Wisconsin--Madison

### Project Description

In 1933, the US government established the Production Credit Associations (PCA), a system of cooperative banks within the Farm Credit System for lending short term operating loans directly to farmers.  The Farm Credit System, (FCS) a government sponsored enterprise established in 1917, was a reaction to a perceived need for credit in US agriculture that commercial banks would or could not provide.  Prior to 1933, the only loans directly available to farmers from the FCS were long term mortgages, and the PCA's were an attempt by the US government to supply short term operating loans specifically suited to farmers; unlike other operating loans, PCA loans had flexible terms and maturity dates, seldom required collateral, and were more accessible since PCA's were placed to cover less populated areas.

This project aims to assess whether the establishment of PCA's in 1933 helped farmers to recover from the Dust Bowl by extending production credit at a time when fewer banks were willing to lend.  To this end, this project analyzes the USDA Census of Agriculture data from the period 1920-1940 as well as climate data to assess the effect of PCA proximity on agricultural productivity at the county level.

### Folders
\docs

- Proposals, presentations, and manuscript.

\lib

- Scripts and code in both python and Stata for cleaning and analyzing data

\tables

- Tables and outputs from empirical analysis

### Data Sources
Haines, Michael, Price Fishback, and Paul Rhode. United States Agriculture Data, 1840 - 2010. ICPSR35206-v1. Ann Arbor, MI: Inter-university Consortium for Political and Social Research [distributor], 2014-12-22. http://doi.org/10.3886/ICPSR35206.v1

Farm Credit Administration. "Location of Production Credit Associations 1937"